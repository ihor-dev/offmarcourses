/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */


// Auto margin elements
function auto_margin(wrapper, elements){
  
    var outerW = wrapper.width();
    var count = elements.length;
    elements.css({"margin-left":"0", "margin-right":"0"});
    var innerW = 0;
    elements.each(function(a,b){
      innerW = innerW + jQuery(b).width();
    });
    var marg = Math.floor( ((outerW - innerW) / count) / 2 );

    elements.css({"margin-left": marg, "margin-right": marg});
}

// Auto vertical centering elements
function auto_vertalign(wrapper, element) {
  element.css({"margin-top":"0", "margin-bottom":"0"});
  var outerH = wrapper.height();  
  var innerH = element.height();

  var marg = Math.floor( (outerH - innerH) / 2 );
  if(innerH >= outerH){ return false;}
  element.css({"margin-top": marg});  
}

/*** 
* Set inline width and height to div covered bgimage width imgLiquid
* config: <div class="autoWH" data-ratio="2"><img src="..."/></div>
* result => width:100%; height: 100%/2 = 50%;
*/
function auto_width_height_div() {
  jQuery(".autoWH").each(function(a, b){
      var element = jQuery(b);
      var wrapper = element.parent();
      var ratio = element.data('ratio');
      var outerW = wrapper.width();
      var innerH = outerW / ratio;

      element.animate({"width":outerW, "height":innerH, "display": "block"});
  })
}

// Auto height elements with <div class="autoH"></div>
function autoH(){
  var maxH = 0;
  jQuery(".autoH").each(function(i,b){
    var el = jQuery(b);
    if(el.height() > maxH){
      maxH = el.height();
    }
  });
  jQuery(".autoH").height(maxH);
}


// Mobile menu button
function mobile_menu(){
  jQuery(".mobile-menu-button").click(function(){
    jQuery(".first-line").toggle(450);
  });
}
 

(function($) {

  // Use this variable to set up the common and page specific functions. If you
  // rename this variable, you will also need to rename the namespace below.
  var Sage = {
    // All pages
    'common': {
      init: function() {
        // JavaScript to be fired on all pages


      },
      finalize: function() {
        // JavaScript to be fired on all pages, after page specific JS is fired
          
          //set height calculated on element width
          auto_width_height_div();
          jQuery(window).resize(function(){
            auto_width_height_div();
          });

          //setup the same height to elements
          setTimeout(function(){autoH();},10);
          jQuery(window).resize(function(){
            setTimeout(function(){autoH();},10);
          });

          // ---- Secondary menu ----
          auto_margin(jQuery("#menu-secondary-menu-1"), jQuery("#menu-secondary-menu-1 li"));
          $(window).resize(function(){
            auto_margin(jQuery("#menu-secondary-menu-1"), jQuery("#menu-secondary-menu-1 li"));
          });

          // --- mobile menu ---
          mobile_menu();

          // --- Set img to div bg
          $(".imgbg").imgLiquid({ fill: true, horizontalAlign: "center",  verticalAlign: "center"});
          $(".imgbg-c").imgLiquid({ fill: false, horizontalAlign: "center",  verticalAlign: "center"});

          //vertical centering logo
          auto_vertalign($(".second-line"), $("span.logo"));
          $(window).resize(function(){
            auto_vertalign($(".second-line"), $("span.logo"));
          });
      }
    },
    // Home page
    'home': {
      init: function() {
        // JavaScript to be fired on the home page
         
      },
      finalize: function() {
        // JavaScript to be fired on the home page, after the init JS

        //rounded icons
        auto_vertalign($(".left-side .items.row"), $(".left-side .items .col-xs-5"));
        $(window).resize(function(){
          auto_vertalign($(".left-side .items.row"), $(".left-side .items .col-xs-5"));
        });
      }
    },
    // About us page, note the change from about-us to about_us.
    'about_us': {
      init: function() {
        // JavaScript to be fired on the about us page
      }
    }
  };

  // The routing fires all common scripts, followed by the page specific scripts.
  // Add additional events for more control over timing e.g. a finalize event
  var UTIL = {
    fire: function(func, funcname, args) {
      var fire;
      var namespace = Sage;
      funcname = (funcname === undefined) ? 'init' : funcname;
      fire = func !== '';
      fire = fire && namespace[func];
      fire = fire && typeof namespace[func][funcname] === 'function';

      if (fire) {
        namespace[func][funcname](args);
      }
    },
    loadEvents: function() {
      // Fire common init JS
      UTIL.fire('common');

      // Fire page-specific init JS, and then finalize JS
      $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
        UTIL.fire(classnm);
        UTIL.fire(classnm, 'finalize');
      });

      // Fire common finalize JS
      UTIL.fire('common', 'finalize');
    }
  };


  // Load Events
  $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.
