<?php get_template_part('templates/footer/footer-courses'); ?>

<?php get_template_part('templates/footer/footer-links'); ?>

<?php get_template_part('templates/footer/footer-subscribe'); ?>

<? $ID = icl_object_id(get_id_by_slug("options-page"), 'page', false,ICL_LANGUAGE_CODE); ?>

<footer class="footer">	  
	<div class="container">
		<div class="row widget">
			<div class="col-sm-24">
				<?the_field('footer_content', $ID);?>
			</div>
		</div>
		<div class="row banners">
			<div class="col-sm-24">
			<? if(count(get_field("footer_bunners", $ID))>0):?>
				<ul>
                <? $imgs = get_field("footer_bunners", $ID);  foreach($imgs as $img):?>
					<li>
						<div class="imgbg-c" data-ratio="2"><img src="<?=$img['url'];?>" alt=""></div>
					</li>                    
                <? endforeach;?>
                </ul>
			<? endif;?> 				
			</div>
		</div>
		<div class="row copyright">
			<div class="col-sm-24">
				<div class="text"><?the_field('footer_copyright', $ID);?></div>
			</div>
		</div>
	</div>
</footer>
