<section class="header-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
           
            <?php if(strlen(get_field('search-form_title')) > 0): ?>
                <div class="header-text"><?the_field('search-form_title');?></div>
                <div class="search-form">
                    <input type="text" name="search_text" class="search-text" placeholder="<?the_field('search_input_placeholder');?>">
                    <input type="text" name="search_date" class="search-date half-width-input left-input" placeholder="<?the_field('search_date_placeholder');?>">
                    <input type="text" name="search-country" class="search-country half-width-input" placeholder="<?the_field('search_country_placeholder');?>">
                    <a href="#" class="search-advanced"><?the_field('search_advanced_link_text');?></a>
                    <button type="submit" class="search-submit"><?the_field('search_submit_text');?></button>
                    
                </div>
            <? endif;?>
            </div>
            <div class="clearfix visible-xs"></div>
            <div class="col-sm-12">

                <div class="row header-courses-list">
                    <div class="col-xs-6"><div class="course-logo imgbg-c autoWH" data-ratio="2"><img src="<?= get_template_directory_uri() . '/dist';?>/images/courses_icons_1.png" alt=""></div></div>
                    <div class="col-xs-12">
                        <p class="title">Limited offshore traning</p>
                        <p class="content">FMTC Amsterdam, NL</p>
                        <div class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></div>
                    </div>
                    <div class="col-xs-6">
                        <p class="price">€525,-</p>
                        <div class="button-group"><i class="glyphicon glyphicon-info-sign"></i><button class="book"><? _e('Book', 'sage'); ?></button></div>
                    </div>
                </div>
                <div class="row header-courses-list">
                    <div class="col-xs-6"><div class="course-logo imgbg-c autoWH" data-ratio="2"><img src="<?= get_template_directory_uri() . '/dist';?>/images/courses_icons_2.png" alt=""></div></div>
                    <div class="col-xs-12">
                        <p class="title">Limited offshore traning</p>
                        <p class="content">FMTC Amsterdam, NL</p>
                        <div class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></div>
                    </div>
                    <div class="col-xs-6">
                        <p class="price">€525,-</p>
                        <div class="button-group"><i class="glyphicon glyphicon-info-sign"></i><button class="book"><? _e('Book', 'sage'); ?></button></div>
                    </div>
                </div>
                <div class="row header-courses-list">
                    <div class="col-xs-6"><div class="course-logo imgbg-c autoWH" data-ratio="2"><img src="<?= get_template_directory_uri() . '/dist';?>/images/courses_icons_3.png" alt=""></div></div>
                    <div class="col-xs-12">
                        <p class="title">Limited offshore traning</p>
                        <p class="content">FMTC Amsterdam, NL</p>
                        <div class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></div>
                    </div>
                    <div class="col-xs-6">
                        <p class="price">€525,-</p>
                        <div class="button-group"><i class="glyphicon glyphicon-info-sign"></i><button class="book"><? _e('Book', 'sage'); ?></button></div>
                    </div>
                </div>
                <div class="row header-courses-list">
                    <div class="col-xs-6"><div class="course-logo imgbg-c autoWH" data-ratio="2"><img src="<?= get_template_directory_uri() . '/dist';?>/images/courses_icons_4.png" alt=""></div></div>
                    <div class="col-xs-12">
                        <p class="title">Limited offshore traning</p>
                        <p class="content">FMTC Amsterdam, NL</p>
                        <div class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></div>
                    </div>
                    <div class="col-xs-6">
                        <p class="price">€525,-</p>
                        <div class="button-group"><i class="glyphicon glyphicon-info-sign"></i><button class="book"><? _e('Book', 'sage'); ?></button></div>
                    </div>
                </div>
                <a href="#" class="view-all"><? _e('View all courses', 'sage'); ?> <i class="glyphicon glyphicon-triangle-bottom arr-right"></i></a>
            </div>
        </div>
    </div>
</section>