<section class="latest-news">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="section-title">Our latest news</p>
			</div>
			<div class="col-sm-12">
				<a href="#" class="show-all"><? _e('Show all news', 'sage'); ?><i class="glyphicon glyphicon-triangle-bottom arr-right"></i></a>
			</div>
		</div>

		<div class="row">			
			
			<div class="col-xs-12 col-sm-6 news-col autoH">
				<div class="img-wrapper imgbg autoWH" data-ratio='2'>
					<img src="<?= get_template_directory_uri() . '/dist';?>/images/latest-news-img-1.jpg" alt="">
					<span class="sticker">
						<span class="number">22</span>
						<span class="text">maart</span>
					</span>
				</div>
				<div class="title">Our new website. Consectetur adipisicing elit!</div>
				<div class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos reiciendis deserunt explicabo aspernatur aperiam ratione fuga!</div>
			
				<a href="#" class="read-more"><? _e('Read more', 'sage'); ?> <i class="glyphicon glyphicon-triangle-bottom arr-right"></i></a>
			</div>

			<div class="col-xs-12 col-sm-6 news-col autoH">
				<div class="img-wrapper imgbg autoWH" data-ratio='2'>
					<img src="<?= get_template_directory_uri() . '/dist';?>/images/latest-news-img-2.jpg" alt="">
					<span class="sticker">
						<span class="number">22</span>
						<span class="text">maart</span>
					</span>
				</div>
				<div class="title">Our new website. Consectetur adipisicing elit!</div>
				<div class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos reiciendis deserunt explicabo aspernatur aperiam ratione fuga!</div>
			
				<a href="#" class="read-more"><? _e('Read more', 'sage'); ?>  <i class="glyphicon glyphicon-triangle-bottom arr-right"></i></a>
			</div>

			<div class="col-xs-12 col-sm-6 news-col autoH">
				<div class="img-wrapper imgbg autoWH" data-ratio='2'>
					<img src="<?= get_template_directory_uri() . '/dist';?>/images/latest-news-img-3.jpg" alt="">
					<span class="sticker">
						<span class="number">22</span>
						<span class="text">maart</span>
					</span>
				</div>
				<div class="title">Our new website. Consectetur adipisicing elit!</div>
				<div class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos reiciendis deserunt explicabo aspernatur aperiam ratione fuga!</div>
			
				<a href="#" class="read-more"><? _e('Read more', 'sage'); ?>  <i class="glyphicon glyphicon-triangle-bottom arr-right"></i></a>
			</div>

			<div class="col-xs-12 col-sm-6 news-col autoH">
				<div class="img-wrapper imgbg autoWH" data-ratio='2'>
					<img src="<?= get_template_directory_uri() . '/dist';?>/images/latest-news-img-1.jpg" alt="">
					<span class="sticker">
						<span class="number">22</span>
						<span class="text">maart</span>
					</span>
				</div>
				<div class="title">Our new website. Consectetur adipisicing elit!</div>
				<div class="desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dignissimos reiciendis deserunt explicabo aspernatur aperiam ratione fuga!</div>
			
				<a href="#" class="read-more"><? _e('Read more', 'sage'); ?> <i class="glyphicon glyphicon-triangle-bottom arr-right"></i></a>
			</div>

		</div>
	</div>
</section>