<section class="special-deals">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 left-side">
			<? if(have_rows('special-deals')): ?>
				<p class="section-title"><?the_field('title');?></p>
				 
				<? while(have_rows('special-deals')): the_row(); ?>
				<div class="row items">
					<div class="col-xs-5"><div class="icon-in-round" style="background-color:<?the_sub_field('round_color');?>"><img src="<?the_sub_field('image');?>" alt=""></div></div>
					<div class="col-xs-19">
						<p class="title"><?the_sub_field('title');?></p>
						<p class="desc"><?the_sub_field('description');?></p>
					</div>
				</div>
				<? endwhile;?>
			<? endif;?>
			</div>


			<div class="col-sm-12 right-side">
				<p class="section-title"><?the_field('title-2');?></p>

				<div class="row items">
					<div class="col-xs-8"><div class="img-wrapper imgbg-c"><img src="<?= get_template_directory_uri() . '/dist';?>/images/providers-img-1.jpg" alt=""></div></div>
					<div class="col-xs-16">
						<div class="desc"><b>Redware</b> Amsterdam, The Netherlands</div>
						<div class="links"><a href="#">Maritime</a>, <a href="#">GWO</a> & <a href="#">Offshore</a></div>
						<div class="rating">
							<span class="label">8,5</span>
							<span class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></span>
						</div>						
					</div>
				</div>

				<div class="row items">
					<div class="col-xs-8"><div class="img-wrapper imgbg-c"><img src="<?= get_template_directory_uri() . '/dist';?>/images/providers-img-2.jpg" alt=""></div></div>
					<div class="col-xs-16">
						<div class="desc"><b>Redware</b> Amsterdam, The Netherlands</div>
						<div class="links"><a href="#">Maritime</a>, <a href="#">GWO</a> & <a href="#">Offshore</a></div>
						<div class="rating">
							<span class="label">8,5</span>
							<span class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></span>
						</div>						
					</div>
				</div>

				<div class="row items">
					<div class="col-xs-8"><div class="img-wrapper imgbg-c"><img src="<?= get_template_directory_uri() . '/dist';?>/images/providers-img-3.jpg" alt=""></div></div>
					<div class="col-xs-16">
						<div class="desc"><b>Redware</b> Amsterdam, The Netherlands</div>
						<div class="links"><a href="#">Maritime</a>, <a href="#">GWO</a> & <a href="#">Offshore</a></div>
						<div class="rating">
							<span class="label">8,5</span>
							<span class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></span>
						</div>						
					</div>
				</div>

				<div class="row items">
					<div class="col-xs-8"><div class="img-wrapper imgbg-c"><img src="<?= get_template_directory_uri() . '/dist';?>/images/providers-img-2.jpg" alt=""></div></div>
					<div class="col-xs-16">
						<div class="desc"><b>Redware</b> Amsterdam, The Netherlands</div>
						<div class="links"><a href="#">Maritime</a>, <a href="#">GWO</a> & <a href="#">Offshore</a></div>
						<div class="rating">
							<span class="label">8,5</span>
							<span class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></span>
						</div>						
					</div>
				</div>

				<div class="row items">
					<div class="col-xs-8"><div class="img-wrapper imgbg-c"><img src="<?= get_template_directory_uri() . '/dist';?>/images/providers-img-1.jpg" alt=""></div></div>
					<div class="col-xs-16">
						<div class="desc"><b>Redware</b> Amsterdam, The Netherlands</div>
						<div class="links"><a href="#">Maritime</a>, <a href="#">GWO</a> & <a href="#">Offshore</a></div>
						<div class="rating">
							<span class="label">8,5</span>
							<span class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></span>
						</div>						
					</div>
				</div>
				<a href="#" class="show-all"><? _e('Show all providers', 'sage'); ?> <i class="glyphicon glyphicon-triangle-bottom arr-right"></i></a>
			</div>
		</div>
	</div>
</section>