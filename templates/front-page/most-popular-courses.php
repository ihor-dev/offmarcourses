<section class="most-popular-courses">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="section-title"><? _e('View most popular courses', 'sage'); ?> </p>
			</div>
			<div class="col-sm-12">
				<a href="#" class="view-all"><? _e('View all courses', 'sage'); ?> <i class="glyphicon glyphicon-triangle-bottom arr-right"></i></a>
			</div>
		</div>

		<div class="row second-row">
			<!--// course col -->
			<div class="col-xs-8">
				<div class="course-thumb imgbg">
					<img src="<?= get_template_directory_uri() . '/dist';?>/images/courses-thumb-1.jpg" alt="">
					<span class="title">Offshore</span>
					<span class="link"><? _e('Show all courses', 'sage'); ?></span>
				</div>

				<!-- course item -->
				<div class="course-item">
					<div class="row">
						<div class="col-sm-18">
							<p class="title">Dynamic Positioning Awarness</p>
							<p class="descr">FMTC Amsterdam</p>
						</div>
						<div class="col-sm-6"><span class="price">€485,-</span></div>
					</div>
					<div class="row">
						<div class="col-sm-13">
							<span class="label">8,3</span>
							<span class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></span>
						</div>
						<div class="col-sm-11">
							<div class="button-group"><i class="glyphicon glyphicon-info-sign"></i><button class="book"><? _e('Book', 'sage'); ?></button></div>
						</div>
					</div>
				</div>
				<!--// course item -->

				<!-- course item -->
				<div class="course-item">
					<div class="row">
						<div class="col-sm-18">
							<p class="title">Dynamic Positioning Awarness</p>
							<p class="descr">FMTC Amsterdam</p>
						</div>
						<div class="col-sm-6"><span class="price">€485,-</span></div>
					</div>
					<div class="row">
						<div class="col-sm-13">
							<span class="label">8,3</span>
							<span class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></span>
						</div>
						<div class="col-sm-11">
							<div class="button-group"><i class="glyphicon glyphicon-info-sign"></i><button class="book"><? _e('Book', 'sage'); ?></button></div>
						</div>
					</div>
				</div>
				<!--// course item -->

				<!-- course item -->
				<div class="course-item">
					<div class="row">
						<div class="col-sm-18">
							<p class="title">Dynamic Positioning Awarness</p>
							<p class="descr">FMTC Amsterdam</p>
						</div>
						<div class="col-sm-6"><span class="price">€485,-</span></div>
					</div>
					<div class="row">
						<div class="col-sm-13">
							<span class="label">8,3</span>
							<span class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></span>
						</div>
						<div class="col-sm-11">
							<div class="button-group"><i class="glyphicon glyphicon-info-sign"></i><button class="book"><? _e('Book', 'sage'); ?></button></div>
						</div>
					</div>
				</div>				
				<!--// course item -->								
				<a href="#" class="show-more"><? _e('Show all courses', 'sage'); ?><i class="glyphicon glyphicon-triangle-bottom arr-right"></i></a>
			</div>
			<!--// course col -->

			<!--// course col -->
			<div class="col-xs-8">
				<div class="course-thumb imgbg">
					<img src="<?= get_template_directory_uri() . '/dist';?>/images/courses-thumb-2.jpg" alt="">
					<span class="title">Maritime</span>
					<span class="link">Show all courses</span>
				</div>

				<!-- course item -->
				<div class="course-item">
					<div class="row">
						<div class="col-sm-18">
							<p class="title">Dynamic Positioning Awarness</p>
							<p class="descr">FMTC Amsterdam</p>
						</div>
						<div class="col-sm-6"><span class="price">€485,-</span></div>
					</div>
					<div class="row">
						<div class="col-sm-13">
							<span class="label">8,3</span>
							<span class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></span>
						</div>
						<div class="col-sm-11">
							<div class="button-group"><i class="glyphicon glyphicon-info-sign"></i><button class="book"><? _e('Book', 'sage'); ?></button></div>
						</div>
					</div>
				</div>
				<!--// course item -->

				<!-- course item -->
				<div class="course-item">
					<div class="row">
						<div class="col-sm-18">
							<p class="title">Dynamic Positioning Awarness</p>
							<p class="descr">FMTC Amsterdam</p>
						</div>
						<div class="col-sm-6"><span class="price">€485,-</span></div>
					</div>
					<div class="row">
						<div class="col-sm-13">
							<span class="label">8,3</span>
							<span class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></span>
						</div>
						<div class="col-sm-11">
							<div class="button-group"><i class="glyphicon glyphicon-info-sign"></i><button class="book"><? _e('Book', 'sage'); ?></button></div>
						</div>
					</div>
				</div>
				<!--// course item -->

				<!-- course item -->
				<div class="course-item">
					<div class="row">
						<div class="col-sm-18">
							<p class="title">Dynamic Positioning Awarness</p>
							<p class="descr">FMTC Amsterdam</p>
						</div>
						<div class="col-sm-6"><span class="price">€485,-</span></div>
					</div>
					<div class="row">
						<div class="col-sm-13">
							<span class="label">8,3</span>
							<span class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></span>
						</div>
						<div class="col-sm-11">
							<div class="button-group"><i class="glyphicon glyphicon-info-sign"></i><button class="book"><? _e('Book', 'sage'); ?></button></div>
						</div>
					</div>
				</div>
				<!--// course item -->	
				<a href="#" class="show-more"><? _e('Show all courses', 'sage'); ?><i class="glyphicon glyphicon-triangle-bottom arr-right"></i></a>

			</div>
			<!--// course col -->

			<!--// course col -->
			<div class="col-xs-8">
				<div class="course-thumb imgbg">
					<img src="<?= get_template_directory_uri() . '/dist';?>/images/courses-thumb-3.jpg" alt="">
					<span class="title">GWO</span>
					<span class="link"><? _e('Show all courses', 'sage'); ?></span>
				</div>

				<!-- course item -->
				<div class="course-item">
					<div class="row">
						<div class="col-sm-18">
							<p class="title">Dynamic Positioning Awarness</p>
							<p class="descr">FMTC Amsterdam</p>
						</div>
						<div class="col-sm-6"><span class="price">€485,-</span></div>
					</div>
					<div class="row">
						<div class="col-sm-13">
							<span class="label">8,3</span>
							<span class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></span>
						</div>
						<div class="col-sm-11">
							<div class="button-group"><i class="glyphicon glyphicon-info-sign"></i><button class="book"><? _e('Book', 'sage'); ?></button></div>
						</div>
					</div>
				</div>
				<!--// course item -->

				<!-- course item -->
				<div class="course-item">
					<div class="row">
						<div class="col-sm-18">
							<p class="title">Dynamic Positioning Awarness</p>
							<p class="descr">FMTC Amsterdam</p>
						</div>
						<div class="col-sm-6"><span class="price">€485,-</span></div>
					</div>
					<div class="row">
						<div class="col-sm-13">
							<span class="label">8,3</span>
							<span class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></span>
						</div>
						<div class="col-sm-11">
							<div class="button-group"><i class="glyphicon glyphicon-info-sign"></i><button class="book"><? _e('Book', 'sage'); ?></button></div>
						</div>
					</div>
				</div>
				<!--// course item -->

				<!-- course item -->
				<div class="course-item">
					<div class="row">
						<div class="col-sm-18">
							<p class="title">Dynamic Positioning Awarness</p>
							<p class="descr">FMTC Amsterdam</p>
						</div>
						<div class="col-sm-6"><span class="price">€485,-</span></div>
					</div>
					<div class="row">
						<div class="col-sm-13">
							<span class="label">8,3</span>
							<span class="stars"><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i><i class="glyphicon glyphicon-star"></i></span>
						</div>
						<div class="col-sm-11">
							<div class="button-group"><i class="glyphicon glyphicon-info-sign"></i><button class="book"><? _e('Book', 'sage'); ?></button></div>
						</div>
					</div>
				</div>
				<!--// course item -->	

				<a href="#" class="show-more"><? _e('Show all courses', 'sage'); ?> <i class="glyphicon glyphicon-triangle-bottom arr-right"></i></a>							
			</div>
			<!--// course col -->						

		</div>
	</div>
</section>