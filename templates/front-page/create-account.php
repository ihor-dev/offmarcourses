
<section class="create-account imgbg">
	<img src="<?= get_template_directory_uri() . '/dist';?>/images/section-create-account-bg.jpg" alt="">
	<div class="container">
		<div class="row">
			<div class="col-sm-18 col-xs-24">
				<p class="section-title text-wh"><?the_field('cr-acc-title');?></p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-10">
				<?the_field('cr-acc-content');?>
				<button class="btn big green"><?the_field('cr-acc-button');?></button>
			</div>
			<div class="col-sm-14">
		
			</div>
		</div>
	</div>
	<div class="img-wrapper imgbg-c" data-imgLiquid-verticalAlign="100%">
		<img src="<?the_field('cr-acc-image');?>" alt="">
	</div>
</section>