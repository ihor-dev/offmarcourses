<header class="banner">    
    <section class="first-line">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <nav class="left-topnav">
                        <div class="btn-group">
                            <a class="dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="glyphicon glyphicon-triangle-bottom"></i>Plan and book courses</a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                        <div class="btn-group">                
                            <a class="dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="glyphicon glyphicon-triangle-bottom"></i>Manage your booking</a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Action</a></li>
                                <li><a href="#">Another action</a></li>
                                <li><a href="#">Something else here</a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Separated link</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
                <div class="col-sm-12">
                    <nav class="nav-primary">

                        <?php
                        if (has_nav_menu('primary_navigation')) :
                            wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav', 'container' => 'div', 'container_class' => 'menu-primary-menu-container']);
                        endif;
                        ?>
                        <ul class="account-menu nav">
                            <li><a href="#">Register</a></li>
                            <li><a href="#">Login <i class="glyphicon glyphicon-log-in"></i></a></li>
                        </ul>
                        
                        <?php
                        if (has_nav_menu('secondary_navigation')) :
                            wp_nav_menu(['theme_location' => 'secondary_navigation', 'menu_class' => 'nav visible-xs']);
                        endif;
                        ?>
                    </nav>
                </div>
            </div>
        </div>
    </section>
    <section class="second-line">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 logo-wrapper">
                    <span class="logo"><b>offmar</b>courses</span>
                    <div class="mobile-menu-button visible-xs">
                        <span class="line"></span>
                        <span class="line"></span>
                        <span class="line"></span>
                    </div>
                </div>
                <div class="col-sm-12">
                    <?php
                    if (has_nav_menu('secondary_navigation')) :
                        wp_nav_menu(['theme_location' => 'secondary_navigation', 'container' => 'false', 'menu_class' => 'tabs-nav hidden-xs']);
                    endif;
                    ?>
                </div>
            </div>
        </div>
    </section>    
</header>
