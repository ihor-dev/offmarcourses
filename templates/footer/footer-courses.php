<section class="footer-courses imgbg">
	<a href="#"><img src="<?= get_template_directory_uri() . '/dist';?>/images/footer-bg-1.jpg"></a>
	<div class="container">
		<div class="row">
			
			<!-- courses col -->
			<div class="col-xs-8 footer-courses-col">
				<div class="header">
					<div class="title">Offshore courses</div>
					<div class="desc">New solutions, combined with best support. Lorem ipsum dolor sit amet.</div>
				</div>
				
				<div class="course-item">
					<div class="title">Lorem ipsum dolor sit amet.</div>
					<span class="location">FMTC Amsterdam</span>
					<a href="#" class="link">More information</a>
				</div>
				<div class="course-item">
					<div class="title">Lorem ipsum dolor sit amet.</div>
					<span class="location">FMTC Amsterdam</span>
					<a href="#" class="link">More information</a>
				</div>
				<div class="course-item">
					<div class="title">Lorem ipsum dolor sit amet.</div>
					<span class="location">FMTC Amsterdam</span>
					<a href="#" class="link">More information</a>
				</div>
				<div class="course-item">
					<div class="title">Lorem ipsum dolor sit amet.</div>
					<span class="location">FMTC Amsterdam</span>
					<a href="#" class="link">More information</a>
				</div>
				<div class="course-item">
					<div class="title">Lorem ipsum dolor sit amet.</div>
					<span class="location">FMTC Amsterdam</span>
					<a href="#" class="link">More information</a>
				</div>

				<a href="#" class="show-more"><? _e('Show more courses', 'sage'); ?> <i class="glyphicon glyphicon-triangle-bottom arr-right"></i></a>
			</div>
			<!--// courses col -->

			<!-- courses col -->
			<div class="col-xs-8 footer-courses-col">
				<div class="header">
					<div class="title">Offshore courses</div>
					<div class="desc">New solutions, combined with best support. Lorem ipsum dolor sit amet.</div>
				</div>
				
				<div class="course-item">
					<div class="title">Lorem ipsum dolor sit amet.</div>
					<span class="location">FMTC Amsterdam</span>
					<a href="#" class="link">More information</a>
				</div>
				<div class="course-item">
					<div class="title">Lorem ipsum dolor sit amet.</div>
					<span class="location">FMTC Amsterdam</span>
					<a href="#" class="link">More information</a>
				</div>
				<div class="course-item">
					<div class="title">Lorem ipsum dolor sit amet.</div>
					<span class="location">FMTC Amsterdam</span>
					<a href="#" class="link">More information</a>
				</div>
				<div class="course-item">
					<div class="title">Lorem ipsum dolor sit amet.</div>
					<span class="location">FMTC Amsterdam</span>
					<a href="#" class="link">More information</a>
				</div>
				<div class="course-item">
					<div class="title">Lorem ipsum dolor sit amet.</div>
					<span class="location">FMTC Amsterdam</span>
					<a href="#" class="link">More information</a>
				</div>

				<a href="#" class="show-more"><? _e('Show more courses', 'sage'); ?><i class="glyphicon glyphicon-triangle-bottom arr-right"></i></a>
			</div>
			<!--// courses col -->

			<!-- courses col -->
			<div class="col-xs-8 footer-courses-col">
				<div class="header">
					<div class="title">Offshore courses</div>
					<div class="desc">New solutions, combined with best support. Lorem ipsum dolor sit amet.</div>
				</div>
				
				<div class="course-item">
					<div class="title">Lorem ipsum dolor sit amet.</div>
					<span class="location">FMTC Amsterdam</span>
					<a href="#" class="link">More information</a>
				</div>
				<div class="course-item">
					<div class="title">Lorem ipsum dolor sit amet.</div>
					<span class="location">FMTC Amsterdam</span>
					<a href="#" class="link">More information</a>
				</div>
				<div class="course-item">
					<div class="title">Lorem ipsum dolor sit amet.</div>
					<span class="location">FMTC Amsterdam</span>
					<a href="#" class="link">More information</a>
				</div>
				<div class="course-item">
					<div class="title">Lorem ipsum dolor sit amet.</div>
					<span class="location">FMTC Amsterdam</span>
					<a href="#" class="link">More information</a>
				</div>
				<div class="course-item">
					<div class="title">Lorem ipsum dolor sit amet.</div>
					<span class="location">FMTC Amsterdam</span>
					<a href="#" class="link">More information</a>
				</div>

				<a href="#" class="show-more"><? _e('Show more courses', 'sage'); ?><i class="glyphicon glyphicon-triangle-bottom arr-right"></i></a>
			</div>
			<!--// courses col -->

		</div>
	</div>
</section>