<section class="subscribe imgbg">
	<a href="#"><img src="<?= get_template_directory_uri() . '/dist';?>/images/footer-bg-2.jpg"></a>
	
	<? $ID = icl_object_id(get_id_by_slug("options-page"), 'page', false,ICL_LANGUAGE_CODE); ?>
	
	<div class="container">
		<div class="row">
			<div class="col-sm-13 left-side">
			<?php if(strlen(get_field('subscr-title', $ID)) > 0) : ?>
				<p class="title"><?the_field('subscr-title', $ID);?></p>
				<p class="desc"><?the_field('subscr-content', $ID);?></p>
				<form action="#">
					<input type="email" name="email" class="inpt big email" placeholder="<?the_field('subscr-input-placeholder', $ID);?>">
					<button type="submit" class="btn middle green"><?the_field('subscr-button', $ID);?></button>
				</form>
			<? endif;?>
			</div>
			<div class="col-sm-11 right-side">
				<? if(have_rows('subscr-right_list', $ID)): ?>
					<? while(have_rows('subscr-right_list', $ID)): the_row(); ?>
						<div class="list-item">
							<i class="glyphicon glyphicon-ok icon"></i>
							<p class="title"><?the_sub_field('list-title');?></p>
							<p class="desc"><?the_sub_field('list-desc');?></p>
						</div>
					<? endwhile;?>
				<? endif;?>
			</div>
		</div>
	</div>
</section>