<section class="footer-links">
	<div class="container">
		<div class="row">
		
		<? $ID = icl_object_id(get_id_by_slug("options-page"), 'page', false,ICL_LANGUAGE_CODE); ?>		

		<? if(have_rows('footer-link-1', $ID)): ?>
			<ul class="links-row">
				<? while(have_rows('footer-link-1', $ID)): the_row(); ?>
					<li><a href="<?the_sub_field('link_url');?>"><?the_sub_field('link_title');?></a></li>
				<? endwhile;?>				
			</ul>
		<? endif;?>
		<? if(have_rows('footer-link-2', $ID)): ?>
			<ul class="links-row">
				<? while(have_rows('footer-link-2', $ID)): the_row(); ?>
					<li><a href="<?the_sub_field('link_url');?>"><?the_sub_field('link_title');?></a></li>
				<? endwhile;?>				
			</ul>
		<? endif;?>
		<? if(have_rows('footer-link-3', $ID)): ?>
			<ul class="links-row">
				<? while(have_rows('footer-link-3', $ID)): the_row(); ?>
					<li><a href="<?the_sub_field('link_url');?>"><?the_sub_field('link_title');?></a></li>
				<? endwhile;?>				
			</ul>
		<? endif;?>
		<? if(have_rows('footer-link-4', $ID)): ?>
			<ul class="links-row">
				<? while(have_rows('footer-link-4', $ID)): the_row(); ?>
					<li><a href="<?the_sub_field('link_url');?>"><?the_sub_field('link_title');?></a></li>
				<? endwhile;?>				
			</ul>
		<? endif;?>
		<? if(have_rows('footer-link-5', $ID)): ?>
			<ul class="links-row">
				<? while(have_rows('footer-link-5', $ID)): the_row(); ?>
					<li><a href="<?the_sub_field('link_url');?>"><?the_sub_field('link_title');?></a></li>
				<? endwhile;?>				
			</ul>
		<? endif;?>				

		</div>
	</div>
</section>